# Ludens.ProtopiaEcosystem react module

<p align="center">
  <a href="https://nact.xyz/" target="blank"><img src="https://gitlab.com/uploads/-/system/project/avatar/52121681/pe_rpg_logo.png" width="100" alt="Ludens Logo" /></a>
</p> 

 PE-module for [ProtopiaEcosystem React Client](https://protopiahome-public.gitlab.io/protopia-ecosystem/cra-template-pe/).
 Includes a live action educational game control system and a basic participant interfaces.

![React](https://img.shields.io/badge/-React-black?style=flat-square&logo=react)
![HTML5](https://img.shields.io/badge/-HTML5-E34F26?style=flat-square&logo=html5&logoColor=white)
![CSS3](https://img.shields.io/badge/-CSS3-1572B6?style=flat-square&logo=css3)
![Bootstrap](https://img.shields.io/badge/-Bootstrap-563D7C?style=flat-square&logo=bootstrap)
![TypeScript](https://img.shields.io/badge/-TypeScript-007ACC?style=flat-square&logo=typescript)
![GraphQL](https://img.shields.io/badge/-GraphQL-E10098?style=flat-square&logo=graphql)
![Apollo GraphQL](https://img.shields.io/badge/-Apollo%20GraphQL-311C87?style=flat-square&logo=apollo-graphql)


## Includes

1. Administrator panel View

2. A graphical representation template for child model screens that extend the module with additional functionality. For a list of existing PE-Ludens child modules, see the PE-documentation

3. Player resource monitor. The element of the user's graphical interface that includes an indicator of the current game time (the number of the current game cycle, its duration and the time remaining until the end)

## Availability

1. ProtopiaEcosystem-react-client v.>2.0
2. WP-server v.>6.0 with PE-plugins:
    - PECore plugin
    - PELudens plugin

## Manual installation and configuration 

1. Install new react client ProtopiaEcosystem. Or open an existing development

2. Start install module:
> `npm run addmodule react-pe-ludens-module https://gitlab.com/protopiahome-public/protopia-ecosystem/react-pe-ludens-module`

3. Wait for the installation to complete.

4. By default, all graphic panels and screen access menus are already registered. But if your client is configured in local or static content display mode, you can optionally change the location of the access buttons to these screens in the config/layout.jsion file

5. Build the client 
> `npm run build`

6. deploy it on your server
